<?php

class Banner_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function getBannerSlots() {
        $Query = $this->db->get("BannerSlot");
        return $Query->result_array();
    }
    public function getValidBannerSlots() {
        $Query = $this->db->get_where("BannerSlot", array("Valid" => 'Y'));
        return $Query->result_array();
    }
    public function getBannerContentByBannerSlotID($BannerSlotID) {
        //Query a random bannercontentid based on the bannerslotid
       $Query="SELECT BannerContentID 
                    FROM ContentToBannerSlot, BannerSlot
                    WHERE ContentToBannerSlot.BannerSlotID='".$BannerSlotID."'
                    AND   ContentToBannerSlot.BannerSlotID=BannerSlot.BannerSlotID
                    AND   BannerSlot.Valid='Y'  
                    ORDER BY RAND()
                    limit 1" ; 
       //execute query
       $ResultSet= $this->db->query($Query);
       $ResultArray= $ResultSet->result_array();
       //Check if the db returned a bannercontentid
       if(isset($ResultArray[0]["BannerContentID"])){
           //Get the bannercontent based on the bannercontentid
           $Query = $this->db->get_where("BannerContent", array("BannerContentID" => $ResultArray[0]["BannerContentID"])); 
           $ResultArray=$Query->result_array();
           return $ResultArray[0]["BannerContent"];
       }
    }
    public function addBannerSlot($Name, $Author) {
        return $this->db->insert("BannerSlot", array("Name" => $Name, "Author" => $Author));
    }
    public function UpdateBannerSlot($BannerSlotID, $Validity){
        $this->db->where('BannerSlotID', $BannerSlotID);
        $this->db->update('BannerSlot',array("Valid" => $Validity));
    }
    public function addBannerContentToBannerSlots($BannerName, $BannerContent, $BannerSlotIDArray) {
        //Rows addedd to the table BannerContentToBannerSlots
        $Rows_added = 0;
        //Check if the bannerslotid array contains anything
        //We can not add content to a bannerslot if there are no bannerslot
        if (sizeof($BannerSlotIDArray) > 0) {
            //STEP 1 ADD THE BANNER CONTENT
            //Add the bannercontent to the banner content table, it returns the boolean value BannerContent_Added.
            $BannerContent_Added = $this->db->insert("BannerContent", array("Name" => $BannerName, "BannerContent" => $BannerContent));
            if ($BannerContent_Added) {
                $BannerContentID = $this->db->insert_id();
            //STEP 2 ADD THE BANNERCONTENTID together with all bannerslotid's to the database    
                //Loop over every bannerslotid
                for ($i = 0; $i < sizeof($BannerSlotIDArray); $i++) {
                    $Added = $this->db->insert("ContentToBannerSlot", array("BannerContentID" => $BannerContentID, "BannerSlotID" => $BannerSlotIDArray[$i]));
                    //After succesfully inserting a row to the contenttobannerslot increase the row count
                    if ($Added) {
                        $Rows_added++;
                    }
                }
            }
        }
        //If the row count is higher than 0, than there is a success.
        if (sizeof($Rows_added) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function check_name_bannerslot($Name) {
        $Query = $this->db->get_where('BannerSlot', array("Name" => $Name), 1);
        return $Query->result_array();
    }

}

?>
