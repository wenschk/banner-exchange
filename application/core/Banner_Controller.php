<?php
include_once(BASEPATH."/core/Controller.php");
class Banner_controler extends CI_Controller {
        
        protected $data=array();
        protected $page;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('banner_model');
                $this->page=strtolower(get_class($this));
                $this->load->helper('url');
                $this->data['title'] = ucfirst($this->page); // Capitalize the first letter
                $this->data['base_url'] = ucfirst(base_url());        
	}        
        
}
?>
