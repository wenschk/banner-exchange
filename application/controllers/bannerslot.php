<?php

//Include the banner controller
include_once(APPPATH . "/core/banner_controller.php");

class Bannerslot extends Banner_controler {

    public function add() {
        //SET FORM VALIDATION    
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->lang->load('form_validation', 'english');
        //Set FORM RULES
        $this->form_validation->set_rules('Name', 'Name', 'trim|required|min_length[5]|max_length[50]|xss_clean|callback_name_check');
        $this->form_validation->set_rules('Author', 'Author', 'trim|required|min_length[5]|max_length[50]|xss_clean');

        $this->load->view('templates/header', $this->data);
        //If the form validation went OK
        if ($this->form_validation->run() == true) {
            //Add the banner slot
            $Row_Added = $this->banner_model->addBannerSlot($this->input->post("Name"), $this->input->post("Author"));
            //If the bannerslot has been added correctly
            if ($Row_Added == TRUE) {
                $this->load->view("success/bannerslot_added", $this->data);
            } else {
                $this->load->view("error/default");
            }
        } else {
            //The form has not been submitted yet or the validation is incorrect
            $this->load->view("forms/bannerslot_add");
        }
        $this->load->view('templates/footer');
    }
    public function edit() {

        $this->load->helper('url');
        $this->data['BannerSlots'] = $this->banner_model->getBannerSlots();
        $this->load->view('templates/header', $this->data);

//Check if there are any Bannerslots available before proceeding

        if (sizeof($this->data['BannerSlots']) < 1) {
            //Display an error message if no bannerslots have been found.
            $this->data['error'] = "No Bannerslots found.";
            unset($this->data['BannerSlots']);
            $this->load->view("error/default", $this->data);
        } else {

            $this->load->view("bannerslot_edit", $this->data);
        }
        $this->load->view('templates/footer');
    }
   public function post_edit() {
        $Validity = $this->input->post("Validity");
        $BannerSlotID = $this->input->post("BannerSlotID");

        if (!empty($Validity) && !empty($BannerSlotID)) {
            $this->banner_model->UpdateBannerSlot($BannerSlotID, $Validity);
            $this->load->view("success/edit_bannerslot");
        }
    }
    public function name_check($str) {
        //This is a callback function for the form validation
        //Check if the bannerslot name already exists in the database
        $ResultArray = $this->banner_model->check_name_bannerslot($str);
        //If the resultset is bigger than zero
        if (sizeof($ResultArray) > 0) {       //set a the form validation message
            $this->form_validation->set_message('name_check', 'The value "' . $str . '" for the field %s already exists in the database');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}

?>
