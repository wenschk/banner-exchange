<?php

class Banner extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('banner_model');
    }

    public function view($BannerSlotID) {
        
        if(isset($BannerSlotID)){
        //Get the bannercontent fron the db
            $data['BannerContent'] = $this->banner_model->getBannerContentByBannerSlotID($BannerSlotID);
        //Fix the bannercontent quotes and double quotes and breaklines    
            $data['BannerContent'] = preg_replace('~[\r\n]+~', '<br />', $data['BannerContent']);
            $data['BannerContent'] = str_replace("'", "\'", $data['BannerContent']);
        }
        $this->load->view("banner", $data);
    }

}

?>
