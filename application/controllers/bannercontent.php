<?php
//Include the banner controller
include_once(APPPATH."/core/banner_controller.php");
class Bannercontent extends Banner_controler {

	public function view()
	{
           $this->load->helper('form'); 
           $this->data['BannerSlots'] = $this->banner_model->getValidBannerSlots();
           $this->load->view('templates/header', $this->data);
//Check if there are any Bannerslots available before proceeding
 //A bannerslot is needed in order to add bannercontent.
           if(sizeof($this->data['BannerSlots'])<1){
               $this->data['error']="No Bannerslots found.";
               unset($this->data['BannerSlots']);
               $this->load->view("error/default", $this->data);
           }else{
            //SET FORM VALIDATION    
                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                $this->lang->load('form_validation', 'english');          
                //SET FORM RULES
                $this->form_validation->set_rules('Name', 'Name', 'trim|required|min_length[5]|max_length[50]|xss_clean|callback_name_check');
                $this->form_validation->set_rules('Bannerslot', 'Bannerslot', 'required');                
                $this->form_validation->set_rules('BannerContent', 'BannerContent', 'required|min_length[5]');
                //IF the form has been validated 
                if($this->form_validation->run()==TRUE){
                    //Add the bannercontent to all designated bannerslots.
                    $BannerContentToSlots_Added= $this->banner_model->addBannerContentToBannerSlots($this->input->post("Name"),$this->input->post("BannerContent"), $this->input->post("Bannerslot"));
                    //Check if it has been added correctly to the database
                    if($BannerContentToSlots_Added==true){
                        $this->load->view("success/bannercontent_added", $this->data);
                    }else{
                        $this->load->view("error/default");
                    }
                    
                }else{
                    //The form has not been submitted yet or the validation is incorrect
                    $this->load->view("forms/".$this->page, $this->data);
                }
                
                
           }
           $this->load->view('templates/footer');
                
	}
         
        
}
?>
