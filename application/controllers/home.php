<?php
//Include the banner controller
include_once(APPPATH . "/core/banner_controller.php");
class Home extends Banner_controler {

    public function view() {

        if (!file_exists(APPPATH . 'views/' . $this->page . '.php')) {
            show_404();
        }

        $this->load->view('templates/header', $this->data);
        $this->data['BannerSlots'] = $this->banner_model->getValidBannerSlots();
  //Check if there are any Bannerslots available before proceeding
 //A bannerslot is needed in order to add bannercontent.
           if(sizeof($this->data['BannerSlots'])<1){
               $this->data['error']="No Bannerslots found.";
               unset($this->data['BannerSlots']);
               $this->load->view("error/default", $this->data);
           }else{      
               $this->load->view($this->page, $this->data);
           }
       $this->load->view('templates/footer');    
    }

}

?>
