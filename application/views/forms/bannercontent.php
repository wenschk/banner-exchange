<div class="row">
   <div  class ="medium-12 columns">
      <h3 id = "TableHeading">Add banner content to banner slot(s)</h3> 
   </div>
</div>

<div class="row">
    <div  class ="medium-12 columns">
    <div class ="panel">
        <form action="" method="POST">
        <div class="row">
            <div  class ="medium-6 columns">
               <label>Name <span class="small">Add a unique name</span></label> 
            </div>
           <div  class ="medium-6 columns">
               <input type="text" name="Name" required>
               <?php echo form_error('Name'); ?>
            </div>
       </div>     
       <div class="row">
            <div  class ="medium-6 columns">
                <label>Choose a banner slot:    <span class="small">Hold down the Ctrl (windows) / Command (Mac) button to select multiple options.</span></label>
            </div>
           <div  class ="medium-6 columns">
                <select name='Bannerslot[]' multiple='multiple' required>
                <?php 
                   $Output="";
                   for ($i = 0; $i < sizeof($BannerSlots); $i++) {
                       $Output.="<option value='" . $BannerSlots[$i]["BannerSlotID"] . "' >" . $BannerSlots[$i]["Name"] . "</option>";
                   } 
                   echo $Output;
                ?>    
                </select>
                <?php echo form_error('Bannerslot'); ?>
            </div>
       </div>  
            
       <div class="row">
            <div  class ="medium-6 columns">
                <label>Add the content <span class="small">The output will be created in the preview window</span></label>
            </div>
           <div  class ="medium-6 columns">
               <textarea  rows="8" cols="50" id="BannerContent" name="BannerContent" required></textarea>
               <?php echo form_error('BannerContent'); ?>
            </div>
       </div>     
      <div class="row">
             <div  class ="medium-10 small-8 columns">
             </div>
            <div  class ="medium-2 small-4 columns">
               <input class="submit"type="submit" value="Add" />
            </div>
      </div>    
      </form> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div  class ="medium-12 columns">
      <div id='PreviewHeader'>Preview</div>
   </div>
</div>
<div class="row">
   <div  class ="medium-12 columns">
<div id="bannerPreview"></div>
   </div>
</div>
<script type="text/javascript"> 
    $(document).ready(function() {        
        $("#BannerContent").bind("keyup  paste", function(event ) {
            var BannerContent=$("#BannerContent").val();
            $("#bannerPreview").html(BannerContent);    
       
        });
    })
</script>