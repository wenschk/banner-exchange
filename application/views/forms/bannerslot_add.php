<div class="row">
   <div  class ="medium-12 columns">
      <h3 id = "TableHeading">Add a banner slot</h3> 
   </div>
</div>
<div class="row">
    <div  class ="medium-12 columns">
    <div class ="panel">
        <form action="" method="POST">
         <div class="row">
            <div  class ="medium-6 columns">
               <label>Name <span class="small">Add a unique name</span></label> 
            </div>
             <div  class ="medium-6 columns">
               <input type="text" name="Name" required value="<?php echo set_value('Name'); ?>"> 
               <?php echo form_error('Name'); ?>
             </div>
        </div>   
         <div class="row">
            <div  class ="medium-6 columns">
               <label>Author<span class="small">Who is the author?</span></label>
            </div>
             <div  class ="medium-6 columns">
               <input type="text" name="Author" value="<?php echo set_value('Author'); ?>" required>
               <?php echo form_error('Author'); ?>
             </div>
        </div>           
         <div class="row">
             <div  class ="medium-10 small-8 columns">
             </div>
            <div  class ="medium-2 small-4 columns">
               <input class="submit"type="submit" value="Add" />
            </div>
        </div>      
        </form> 
     </div>
    </div>
</div>