<!doctype html>
<html class="no-js" lang="en">
    <head>
        <title>Banner Exchange</title>
        <meta name="author" content="Kenneth Wensch">
        <meta name="description" content="Extremia, Assignment">
        <meta name="keywords" content="Extremia, Assignment, PHP, MySQl, HTML,CSS,XML,JavaScript">        
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta charset="UTF-8">
        <link rel="stylesheet" href="<?php  echo  $base_url; ?>css/foundation.css" />       
        <link rel="stylesheet/less" type="text/css" href="<?php  echo  $base_url; ?>less/variables.less" />
        <link rel="stylesheet/less" type="text/css" href="<?php  echo  $base_url; ?>less/style.less">
        <script src="<?php echo $base_url;?>js/vendor/less_v1.7.0.js" type="text/javascript"></script>
        <script src="<?php echo $base_url;?>js/vendor/jquery-1.10.1.min.js"></script>
        <script src="<?php echo $base_url; ?>js/vendor/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?php  echo  $base_url; ?>js/vendor/modernizr.js"></script>
    </head>
    <body>      
        <div class="row">
        <div  class ="medium-12 columns">
                <div class="sticky">
                    <nav class="top-bar" data-topbar="">
                    <ul class="title-area">
                        <li class="name">
                            <h1>
                                <a href="/">Banner Exchange</a>
                            </h1>
                        </li>
                        <li class="toggle-topbar menu-icon">
                            <a href="<?php  echo  $base_url; ?>">Menu</a>
                        </li>
                    </ul>
                    <section class="top-bar-section">
                        <ul class="right">
                            <li class="divider hide-for-small"></li>
                            <li>
                                <a href="<?php  echo  $base_url; ?>index.php/home"> home</a>
                            </li>
                            <li>
                                <a href="<?php  echo  $base_url; ?>index.php/bannerslot/new"> add banner slot</a>
                            </li>
                            <li>
                                <a href="<?php  echo  $base_url; ?>index.php/bannerslot/edit"> edit banner slot</a>
                            </li>                
                            <li>
                                <a href="<?php  echo  $base_url; ?>index.php/bannercontent">add banner content</a>
                            </li> 
                        </ul>
                    </section>
                    </nav>
                </div>
              </div>
              </div>