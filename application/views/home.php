<div class="row">
    <div class="medium-12 columns">
     <h3>Please refresh the page. The content of the banner slots might change.</h3>   
    </div>
</div>

<div class="row">
<?php

$Output="";
//PRINT the bannerslots
$columns = 0;
for($i=0;$i<sizeof($BannerSlots);$i++){
    $BannerSlotID=$BannerSlots[$i]["BannerSlotID"];
    $BannerName=$BannerSlots[$i]["Name"];
    $BannerURL=$base_url."index.php/banner/".$BannerSlotID;
    $Output.="<div class='medium-6 columns'>";
    $Output.="<div id='BoxHeader'>".$BannerName."</div>
                     <div id='BoxBody'>   
                        <script src='".$BannerURL."'></script>
              </div>";
    $Output.="</div>";
    $columns+=6;
    if($columns == 12 && $i<sizeof($BannerSlots)-1){
      $Output.="</div>";
      $Output.="<div class='row'>";
    }
}

echo $Output;
?></div>
