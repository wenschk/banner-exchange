<div class="row">
    <div class="medium-12 columns">
       <div id="InfoBox"></div> 
    </div>
</div>
<div class="row">
    <div class="medium-12 columns">
        <div id='TableHeading'>Edit bannerslot validity</div>
    </div>
</div>

<div class="row">
     <div class="medium-12 columns">
        <table > 
        <thead>
            <tr>
                <th  width="200" >Name <span class='small'>The name of the bannerslot</span></th>
                <th width="150">Author<span class='small'>The name of the author</span></th>
                <th width="150">Validity<span class='small'>Change the validity here.</span></th>
                <th>Copy script tag<span class='small'>Place this script on your website to view the banner slot.</span></th>
            </tr>
        </thead>
        <tbody >
            <?php 

            //CREATE THE ROWS OF THE TABLE
            $Output="";
            for ($i = 0; $i < sizeof($BannerSlots); $i++) {
                //Variables
                $Validity = $BannerSlots[$i]["Valid"];
                $BannerSlotID = $BannerSlots[$i]["BannerSlotID"];
                $BannerSlotName = $BannerSlots[$i]["Name"];
                $Author = $BannerSlots[$i]["Author"];
                $BannerScript = "<script src='" . $base_url . "index.php/banner/" . $BannerSlotID . "'></script>";
                //Â´Start row of a table
                $Output.="<tr>";
                $Output.="<td>" . $BannerSlotName . "</td>";
                $Output.="<td>" . $Author . "</td>";

                //Create the validity selector  
                $Output.="<td><select id='" . $BannerSlotID . "'>";
                if ($Validity == "Y") {
                    $Output.="<option selected>Y</option>";
                    $Output.="<option >N</option>";
                } else {
                    $Output.="<option>Y</option>";
                    $Output.="<option selected>N</option>";
                }
                $Output.="</select></td>";
        //Create the script tag to copy on your website

                $Output.="<td><textarea rows='1' cols='10' onclick='this.focus();this.select()' readonly='readonly' style='resize:none'>";
                $Output.=$BannerScript;
                $Output.="</textarea></td>";
        //close row    
                $Output.="</tr>";
                ;
            }
            echo $Output;

            ?>
        </tbody>
        </table>
    </div>
</div>

<script type="text/javascript"> 
    $(document).ready(function() {        
        $("select").change(function(){
            $.ajax({
                url : "<?php echo $base_url; ?>index.php/bannerslot/edit",
                type: "POST",
                data: "Validity="+$(this).val()+"&BannerSlotID="+$(this).attr("id"),
                success: function(data, textStatus, jqXHR)
                {
                    $("#InfoBox").delay(1000).fadeIn;
                    $("#InfoBox").html("<div class='successbox'>"+data+"</div>"); 
                    $("#InfoBox").show(0).delay(1000).hide(0);;
    
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $("#InfoBox").html("<div class='errorbox'>An error happened</div>"); 
                }
            });
        });
    });

</script>